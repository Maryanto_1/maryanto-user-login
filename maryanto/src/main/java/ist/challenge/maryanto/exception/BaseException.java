package ist.challenge.maryanto.exception;

public class BaseException extends RuntimeException {
    public BaseException(String message) {
        super(message);
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return null;
    }
}
