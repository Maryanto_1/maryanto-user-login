package ist.challenge.maryanto.exception;

public class UsernameOrPasswordNotFoundException extends BaseException {
    public UsernameOrPasswordNotFoundException(String message) {
        super("Username/Password Tidak ditemukan : " + message);
    }
}
