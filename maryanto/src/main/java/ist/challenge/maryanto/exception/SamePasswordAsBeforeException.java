package ist.challenge.maryanto.exception;

public class SamePasswordAsBeforeException extends BaseException {
    public SamePasswordAsBeforeException(String message) {
        super("Password tidak boleh sama dengan password sebelumnya" + message);
    }
}
