package ist.challenge.maryanto.exception;

public class NullUsernameOrPasswordException extends BaseException {
    public NullUsernameOrPasswordException(String message) {
        super("Username dan / atau password kosong ! " + message);
    }
}
