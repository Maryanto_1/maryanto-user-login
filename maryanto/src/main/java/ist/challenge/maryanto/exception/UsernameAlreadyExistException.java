package ist.challenge.maryanto.exception;

public class UsernameAlreadyExistException extends BaseException {
    public UsernameAlreadyExistException(String message) {
        super("Username sudah terpakai : " + message);
    }
}
