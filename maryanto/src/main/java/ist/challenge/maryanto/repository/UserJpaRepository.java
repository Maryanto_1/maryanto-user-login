package ist.challenge.maryanto.repository;

import ist.challenge.maryanto.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserJpaRepository extends JpaRepository<UserEntity, Long>, PagingAndSortingRepository<UserEntity, Long> {
    boolean existsByUsername(String username);
    boolean existsByPassword(String password);
    Optional<UserEntity> findById(Long id);
    void deleteById(Long id);
    Page<UserEntity> findAll(Pageable pageable);

}
