package ist.challenge.maryanto.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEntityRequestLoginDto {
    @NotNull
    @Size(max = 25, message = "Maksimum karakter 25")
    private String username;

    @NotNull
    @Size(max = 255, message = "Maksimum karakter 255")
    private String password;
}
