package ist.challenge.maryanto.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserResponse<T> {
    private T data;

    private String message;

    public static UserResponse success(Object data) {
        return new UserResponse(data, "Success");
    }

    public static UserResponse error(String message) {
        return new UserResponse(null, message);
    }
}
