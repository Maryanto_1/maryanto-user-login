package ist.challenge.maryanto.service;

import ist.challenge.maryanto.dto.UserEntityRequestLoginDto;
import ist.challenge.maryanto.entity.UserEntity;
import ist.challenge.maryanto.exception.NullUsernameOrPasswordException;
import ist.challenge.maryanto.exception.SamePasswordAsBeforeException;
import ist.challenge.maryanto.exception.UsernameAlreadyExistException;
import ist.challenge.maryanto.exception.UsernameOrPasswordNotFoundException;
import ist.challenge.maryanto.repository.UserJpaRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    @Autowired
    private UserJpaRepository userJpaRepository;

    public UserEntity create(UserEntity userEntity) {
        if (!userJpaRepository.existsByUsername(userEntity.getUsername())) {
            return userJpaRepository.save(userEntity);
        } else {
            throw new UsernameAlreadyExistException(userEntity.getUsername());
        }

    }

    public String login(UserEntityRequestLoginDto userEntityRequestDto) {

        if (userEntityRequestDto.getUsername() == null || userEntityRequestDto.getPassword() == null) {
            throw new NullUsernameOrPasswordException("username : " + userEntityRequestDto.getUsername() + ", " +
                    " password : " + userEntityRequestDto.getPassword());
        }

        Boolean checkUsername = userJpaRepository.existsByUsername(userEntityRequestDto.getUsername());
        Boolean checkPassword = userJpaRepository.existsByPassword(userEntityRequestDto.getPassword());

        if (checkUsername == false || checkPassword == false) {
            throw new UsernameOrPasswordNotFoundException(", "+"username : "+userEntityRequestDto.getUsername());
        }
        return "Sukses Login";
    }

    public List<UserEntity> findAll() {
        return userJpaRepository.findAll();
    }

    public Page<UserEntity> findByPagination(int page, int max) {
        Pageable pageable = PageRequest.of(page, max);
        Page<UserEntity> currentPage = userJpaRepository.findAll(pageable);
        return currentPage;
    }

    public UserEntity update(UserEntity userEntity) {
        if (userJpaRepository.findById(userEntity.getId()).isPresent()) {

            if(userJpaRepository.existsByUsername(userEntity.getUsername())) {
                throw new UsernameAlreadyExistException(", " + "username : " +userEntity.getUsername());
            }

            if(userJpaRepository.existsByPassword(userEntity.getPassword())) {
                throw new SamePasswordAsBeforeException(", "+ "password : " +userEntity.getPassword());
            }

            return userJpaRepository.save(userEntity);

        } else {
            throw new UsernameOrPasswordNotFoundException(" : id " + userEntity.getId());
        }

    }

    public void delete(Long  id) {
        userJpaRepository.deleteById(id);
    }

}
