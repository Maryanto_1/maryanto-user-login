package ist.challenge.maryanto.controller;

import io.swagger.v3.oas.annotations.Operation;
import ist.challenge.maryanto.dto.UserEntityRequestDto;
import ist.challenge.maryanto.dto.UserEntityRequestEditDto;
import ist.challenge.maryanto.dto.UserEntityRequestLoginDto;
import ist.challenge.maryanto.entity.UserEntity;
import ist.challenge.maryanto.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private final UserService userService;

    @Autowired
    private final ModelMapper modelMapper;

    @Operation(summary = "Registrasi user baru")
    @PostMapping("/registrasi")
    public ResponseEntity<UserEntity> create(@RequestBody UserEntityRequestDto userEntityRequestDto) {
        UserEntity data = modelMapper.map(userEntityRequestDto, UserEntity.class);
        UserEntity saved = userService.create(data);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                buildAndExpand(saved.getUsername()).toUri();
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Login bagi yang sudah registrasi")
    @PostMapping("/login")
    public String login(@RequestBody UserEntityRequestLoginDto userEntityRequestLoginDto) {
        return userService.login(userEntityRequestLoginDto);
    }

    @Operation(summary = "Melihat seluruh list user")
    @GetMapping("/getAll")
    public List<UserEntity> getAll() {
        return userService.findAll();
    }

    @Operation(summary = "Melihat list user menggunakan pagination")
    @GetMapping("/page/{page}/max/{max}")
    public Page<UserEntity> get(@PathVariable int page, @PathVariable int max) {
        return userService.findByPagination(page, max);
    }

    @Operation(summary = "Mengubah data username dan password")
    @PutMapping("/edit/id")
    public ResponseEntity<UserEntity> update( @RequestBody UserEntityRequestEditDto userEntityRequestEditDto) {
        UserEntity data = modelMapper.map(userEntityRequestEditDto, UserEntity.class);
        UserEntity saved = userService.update(data);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                buildAndExpand(saved.getUsername()).toUri();
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Menghapus data user dengan parameter id")
    @DeleteMapping("/delete/id/{id}")
    public void deleteById(@PathVariable Long id) {
        userService.delete(id);
    }

}
