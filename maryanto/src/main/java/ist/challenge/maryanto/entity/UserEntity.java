package ist.challenge.maryanto.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "USERS")
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 4869606600936128010L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 25, nullable = false, unique = true)
    @Size(max = 25, message = "Maksimum karakter 25")
    private String username;

    @NotNull
    @Column(length = 255, nullable = false)
    @Size(max = 255, message = "Maksimum karakter 255")
    private String password;
}
