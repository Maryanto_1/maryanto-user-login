package ist.challenge.maryanto.configuration;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import ist.challenge.maryanto.dto.UserResponse;
import ist.challenge.maryanto.exception.NullUsernameOrPasswordException;
import ist.challenge.maryanto.exception.SamePasswordAsBeforeException;
import ist.challenge.maryanto.exception.UsernameAlreadyExistException;
import ist.challenge.maryanto.exception.UsernameOrPasswordNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Invalid param value", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponse.class))
            })
    })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {
            NullUsernameOrPasswordException.class, SamePasswordAsBeforeException.class
    })
    protected ResponseEntity<Object> badRequest(RuntimeException ex, WebRequest request) {
        UserResponse body = UserResponse.error(ex.getMessage());
        return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "409", description = "Invalid param value", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponse.class))
            })
    })
    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(value = {UsernameAlreadyExistException.class})
    protected ResponseEntity<Object> conflict(RuntimeException ex, WebRequest request) {
        UserResponse body = UserResponse.error(ex.getMessage());
        return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Invalid param value", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponse.class))
            })
    })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {UsernameOrPasswordNotFoundException.class})
    protected ResponseEntity<Object> notFound(RuntimeException ex, WebRequest request) {
        UserResponse body = UserResponse.error(ex.getMessage());
        return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }


}
